#Mario Cabrera
#Tarea de Vision Computacional
from __future__ import print_function
import cv2 as cv
import numpy as np
import argparse
import random as rng
# Funciones de Sistema Operativo
import os

initSequence=1 # inicio secuencia
numSequences= 10 # fin secuencia
cont_frame = 0 # contador auxiliar para implementar

#Directorio de lectura
path_dataset = "../../jpg"

# Directorios de escritura
path_GRAY = "./Output-Gray"
path_BINARY = "./Output-Binary"
path_EDGES = "./Output-Edges"
path_SEGMENTATION = "./Output-Segmentation"

os.mkdir(path_GRAY)
os.mkdir(path_BINARY)
os.mkdir(path_EDGES)
os.mkdir(path_SEGMENTATION)

totalImages = int(len(os.listdir(path_dataset))) # Contabiliza n�mero de archivos

# Bucle recorre desde el inicio a fin en un rango
cont_frame = 0 # contador auxiliar para implementar
for ns in range(initSequence,numSequences+1):
    cont_frame = cont_frame + 1
    dirimages = path_dataset + '/image_' + str(ns).zfill(4) + '.jpg'
    img = cv.imread(dirimages)

    # Transformar a ESCALA DE GRISES la imagen en color
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    cv.imwrite(path_GRAY + '/grayImage_' + str(ns).zfill(4) + '.jpg', gray)

    #Imagen binaria
    (thresh, binary) = cv.threshold(gray, 128, 255, cv.THRESH_BINARY | cv.THRESH_OTSU)
    cv.imwrite(path_BINARY + '/binaryImage_' + str(ns).zfill(4) + '.jpg', binary)

    #Bordes de una imagen
    bordes = cv.Canny(gray, 90, 700)
    cv.imwrite(path_EDGES + '/edgesImage_' + str(ns).zfill(4) + '.jpg', bordes)

    #Segmentacion imagen
    black = img
    black[np.all(black == 255, axis=2)] = 0
    kernel = np.array([[1, 1, 1], [1, -8, 1], [1, 1, 1]], dtype=np.float32)
    imgLaplacian = cv.filter2D(black, cv.CV_32F, kernel)
    sharp = np.float32(black)
    imgResult = sharp - imgLaplacian
    # convert back to 8bits gray scale
    imgResult = np.clip(imgResult, 0, 255)
    imgResult = imgResult.astype('uint8')
    imgLaplacian = np.clip(imgLaplacian, 0, 255)
    imgLaplacian = np.uint8(imgLaplacian)
    bw = cv.cvtColor(imgResult, cv.COLOR_BGR2GRAY)
    _, bw = cv.threshold(bw, 40, 255, cv.THRESH_BINARY | cv.THRESH_OTSU)
    dist = cv.distanceTransform(bw, cv.DIST_L2, 3)
    cv.normalize(dist, dist, 0, 1.0, cv.NORM_MINMAX)
    _, dist = cv.threshold(dist, 0.4, 1.0, cv.THRESH_BINARY)
    kernel1 = np.ones((3, 3), dtype=np.uint8)
    dist = cv.dilate(dist, kernel1)
    dist_8u = dist.astype('uint8')
    contours, _ = cv.findContours(dist_8u, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    markers = np.zeros(dist.shape, dtype=np.int32)
    for i in range(len(contours)):
        cv.drawContours(markers, contours, i, (i + 1), -1)
    cv.circle(markers, (5, 5), 3, (255, 255, 255), -1)
    cv.watershed(imgResult, markers)
    mark = np.zeros(markers.shape, dtype=np.uint8)
    mark = markers.astype('uint8')
    mark = cv.bitwise_not(mark)
    colors = []
    for contour in contours:
        colors.append((rng.randint(0, 256), rng.randint(0, 256), rng.randint(0, 256)))
    dst = np.zeros((markers.shape[0], markers.shape[1], 3), dtype=np.uint8)
    for i in range(markers.shape[0]):
        for j in range(markers.shape[1]):
            index = markers[i, j]
            if index > 0 and index <= len(contours):
                dst[i, j, :] = colors[index - 1]

    cv.imwrite(path_SEGMENTATION + '/segmentationImage_' + str(ns).zfill(4) + '.jpg', dst)

    print("imagen ",ns," completa")

print(dirimages)
print(cont_frame)
print(totalImages)


