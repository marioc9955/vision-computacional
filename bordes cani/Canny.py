#AUTOR ORIGINAL:Gabriela Solano
#MODIFICADO Y COMENTADO POR: Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :30/11/2020
#REFENRENCIAS:https://docs.opencv.org/
#https://docs.opencv.org/3.1.0/da/d22/tutorial_py_canny.html
# https://omes-va.com/contando-objetos-aplicando-deteccion-de-bordes-con-canny-en-python-opencv/

###IMPORTAR###
import cv2 as cv
import numpy as np

####################-----CODIGO-----#######################
#Leer
imagen = cv.imread('monedas.png')


#Transforma a escala de grises
egrises=cv.cvtColor(imagen,cv.COLOR_BGR2GRAY)

#Deteccion de Bordes
#PARAMETROS de Canny:(Imagen para procesar,umbral bajo,umbral alto)
#Los bordes con valores mas alla del umbral alto se van a dibujar
#Los que se encuentren dentro de los dos rangos se dibujaran unicamente si se encuentran concetados a los bordes detectados
# por el umbral alto
#PARA cartas,cartas3 funciona
bordes=cv.Canny(egrises,90,700)
#CONTORNOS
#Encontramos los contornos de la imagen despues de hallar los bordes
_,ctns,_=cv.findContours(bordes,cv.RETR_EXTERNAL,cv.CHAIN_APPROX_SIMPLE)
#Dibujamos los contornos
cv.drawContours(imagen,ctns,-1,(0,0,255),2)
#Imprimir numero de contornos
print('Numero de contornos encontrados:' ,len(ctns))
#Creamos una variable para visualizar texto
texto='Numero de contornos encontrados:' + str(len(ctns))
#Mostrar texto
cv.putText(imagen,texto,(10,20),cv.FONT_ITALIC,0.7,(255,0,0),1)
#MOSTRAR
#Imagen a procesar
cv.imshow('IMAGEN', imagen)
#Bordes
cv.imshow('Bordes',bordes)


####################----FIN CODIGO----########################

#Cerrar Programa
cv.waitKey(0)
cv.destroyAllWindows()
#FIN