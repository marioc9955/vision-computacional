# INTERFAZ GRÁFICA
from tkinter import *
from tkinter import filedialog
from PIL import Image
from PIL import ImageTk
import imutils

# DETECCION DE ROSTROS
from keras.models import load_model
from keras.preprocessing.image import img_to_array
import cv2
import numpy as np
import os


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


# PROGRAMA
# ----------------------------////----------------------------------
face_classifier = cv2.CascadeClassifier(resource_path(r'haarcascade_frontalface_default.xml'))
classifier = load_model(resource_path(r'Emotion_little_vgg.h5'))
class_labels = ['Enojado', 'Feliz', 'Serio', 'Asombrado']
emojis_img = [cv2.imread(resource_path("enojo.png"), cv2.IMREAD_UNCHANGED),
              cv2.imread(resource_path("risa.png"), cv2.IMREAD_UNCHANGED),
              cv2.imread(resource_path("serio.png"), cv2.IMREAD_UNCHANGED),
              cv2.imread(resource_path("asombro.png"), cv2.IMREAD_UNCHANGED)]

# variable para guardar video
# video_salida = cv2.VideoWriter('videosalida.avi', cv2.VideoWriter_fourcc(*'XVID'), 20.0, (640, 480))
for i in range(0, 4):
    emojis_img[i] = cv2.resize(emojis_img[i], (50, 50), interpolation=cv2.INTER_AREA)


# ----------------------------////----------------------------------
def deteccion_facilal(frame):
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = face_classifier.detectMultiScale(gray, 1.3, 5)

    for (x, y, w, h) in faces:
        frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
        roi_gray = gray[y:y + h, x:x + w]
        roi_gray = cv2.resize(roi_gray, (48, 48), interpolation=cv2.INTER_AREA)
        if np.sum([roi_gray]) != 0:
            roi = roi_gray.astype('float') / 255.0
            roi = img_to_array(roi)
            roi = np.expand_dims(roi, axis=0)
            # PREDICCION
            preds = classifier.predict(roi)[0]
            label = str(class_labels[preds.argmax()] + " " + str(int(preds[preds.argmax()] * 100)) + "%")

            label_position = (x, y)
            cv2.putText(frame, label, label_position, cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 1)

            # colocar emoji con transparency
            x_offset = x
            y_offset = y

            y1, y2 = y_offset, y_offset + emojis_img[preds.argmax()].shape[0]
            x1, x2 = x_offset, x_offset + emojis_img[preds.argmax()].shape[1]

            alpha_s = emojis_img[preds.argmax()][:, :, 3] / 255.0
            alpha_l = 1.0 - alpha_s

            for c in range(0, 3):
                frame[y1:y2, x1:x2, c] = (alpha_s * emojis_img[preds.argmax()][:, :, c] +
                                          alpha_l * frame[y1:y2, x1:x2, c])

        else:
            cv2.putText(frame, 'No Face Found', (20, 60), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0), 3)

    # guardar video
    # video_salida.write(frame)
    return frame


def video_de_entrada():
    global cap
    if selected.get() == 1:
        path_video = filedialog.askopenfilename(filetypes=[
            ("all video format", ".mp4"),
            ("all video format", ".avi")])
        if len(path_video) > 0:
            btnEnd.configure(state="active")
            rad1.configure(state="disabled")
            rad2.configure(state="disabled")
            pathInputVideo = "..." + path_video[-20:]
            lblInfoVideoPath.configure(text=pathInputVideo)
            cap = cv2.VideoCapture(path_video)
            visualizar()
    if selected.get() == 2:
        btnEnd.configure(state="active")
        rad1.configure(state="disabled")
        rad2.configure(state="disabled")
        lblInfoVideoPath.configure(text="")
        cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)
        visualizar()


def visualizar():
    global cap
    ret, frame = cap.read()
    if ret == True:
        frame = imutils.resize(frame, width=640)
        frame = deteccion_facilal(frame)
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        im = Image.fromarray(frame)
        img = ImageTk.PhotoImage(image=im)
        lblVideo.configure(image=img)
        lblVideo.image = img
        lblVideo.after(10, visualizar)
        Instrucciones.configure(text="")


    else:
        lblVideo.image = ""
        lblInfoVideoPath.configure(text="")
        rad1.configure(state="active")
        rad2.configure(state="active")
        selected.set(0)
        btnEnd.configure(state="disabled")
        cap.release()
        Instrucciones.configure(text="")


def finalizar_limpiar():
    lblVideo.image = ""
    lblInfoVideoPath.configure(text="")
    rad1.configure(state="active")
    rad2.configure(state="active")
    selected.set(0)
    cap.release()


cap = None
root = Tk()
root.title("RECONOCIMIENTO GESTUAL DE EMOCIONES ")
root.iconbitmap(resource_path("LOGO RECONOCIMIENTO FACIAL.ico"))
root.config(cursor="hand2", bg="azure")
lblInfo1 = Label(root, text="Detección de Emociones", font="courier", bg="azure")
lblInfo1.grid(column=0, row=0, columnspan=2)
selected = IntVar()
rad1 = Radiobutton(root, text="Elegir video", font="courier", bg="azure", fg="Blue", width=20, value=1,
                   variable=selected, command=video_de_entrada)
rad2 = Radiobutton(root, text="Video en directo", font="courier", bg="azure", fg="Blue", width=20, value=2,
                   variable=selected, command=video_de_entrada)
rad1.grid(column=0, row=1)
rad2.grid(column=1, row=1)
lblInfoVideoPath = Label(root, text="", width=20)
lblInfoVideoPath.grid(column=0, row=2)
Instrucciones = Label(root,
                      text="INSTRUCCIONES: Elija el medio para realizar la detección de emociones.\n Utilice el Botón\n "
                           " <Finalizar visualización y limpiar> \n  para cambiar de video o medio de Visualización.\n"
                           "La Distancia Máxima de percepción es de 52 cm \n", font="courier", bg="lavender", fg="grey",
                      relief="ridge", width=75)
Instrucciones.grid(column=0, row=3, columnspan=2)
lblVideo = Label(root, bg="azure")
lblVideo.grid(column=0, row=4, columnspan=2)
btnEnd = Button(root, text="Finalizar visualización y limpiar", font="courier", fg="Blue", bg="azure", state="disabled",
                command=finalizar_limpiar)
btnEnd.grid(column=0, row=5, columnspan=2, pady=10)
root.mainloop()
